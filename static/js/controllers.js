(function () {

    var app = angular.module("SimpleNoteApp");

    var RootCtrl = function ($scope, $cookies, notesApi, $log, $location) {

        var onError = function (data) {
            $log.info("error while processing request...");
            $log.debug(data);
            if (data.status == 401) {
                // Unauthorized access. it might be that token has been expired.
                // clean up cookie and redirect to login page.
                notesApi.removeToken();
                $location.path("#/login")
            } else {
                $scope.error_message = data;
            }
        };

        var updateScopeNotes = function (data) {
            $scope.notes = data;
        };

        var controllerMain = function () {
            // fetch notes list and update scope with resulting noes.
            $scope.refreshNotes = function () {
                notesApi.getNotesList(token).then(
                    updateScopeNotes,
                    onError
                );
            };
            // on load - update notes list
            $scope.refreshNotes();
        };

        $log.info("Root controller body.");
        // get token, if there is no token - redirect to login page.
        $log.info("Chkecking token...");

        var token = notesApi.getToken();
        $log.debug("Token lookup result: " + token);

        if (!token) {
            $log.info("Redirecting to login since token is not found.");
            $location.path("/login");
        } else {
            controllerMain();
        }

        $scope.logout = function () {
            notesApi.logout();
        };

    };

    var LoginCtrl = function ($scope, $cookies, notesApi, $log, $location) {
        $log.info("Login controller body.");

        var onSuccess = function (data) {
            var token = data.token;
            $log.info("Successful login, token: " + token);
            $cookies.put('notesAppToken', token);
            $log.info("Redirecting to main view.")
            $location.path('/main');
        };

        var onError = function (data) {
            $log.info("Error while login, response: " + data.status + " " + data.statusText);
            if (data.status == 401) {
                $scope.error_message = "Authorization error. Please check credentials";
            } else {
                $scope.error_message = data.statusText || '';
            }
        };

        var successDefaultUserCreation = function (data) {
            $scope.success_message = data.text;
        };

        $scope.login = function (email, passwd) {
            notesApi.login(email, passwd).then(onSuccess, onError);
        };

        $scope.createDefaultUser = function () {
            notesApi.createDefaultUser().then(
                onError,
                successDefaultUserCreation
            );
        };
    };

    var LogOutCtrl = function (notesApi, $log, $location) {
        $log.info("logout control body, invoking logout function");
        notesApi.logout();
        $location.path("/login");
        $log.info("done. you shouldn't be here.")
    };

    app.controller("RootCtrl", RootCtrl);
    app.controller("LoginCtrl", LoginCtrl);
    app.controller("LogOutCtrl", LogOutCtrl);
}());
