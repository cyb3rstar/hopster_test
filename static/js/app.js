(function () {

    var app = angular.module("SimpleNoteApp",
        ["ngRoute", "ngCookies", "ui.bootstrap"]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/main", {
                templateUrl: "partials/main.html",
                controller: "RootCtrl"
            })
            .when("/login", {
                templateUrl: "partials/login.html",
                controller: "LoginCtrl"
            })
            .when("/logout", {
                template: " ",
                controller: "LogOutCtrl"
            })
            .when("/note/create", {
                templateUrl: "partials/create_note.html",
                controller: "CreateNoteCtrl"
            })
            .when("/note/:header", {
                templateUrl: "partials/note.html",
                controller: "GetNoteCtrl"
            })
            .when("/note/:header/update", {
                templateUrl: "partials/update_note.html",
                controller: "UpdateNoteCtrl"
            })
            .when("/note/:header/delete", {
                template: " ",
                controller: "DeleteNoteCtrl"
            })
            .otherwise({redirectTo: "/main"});
    });
}());
