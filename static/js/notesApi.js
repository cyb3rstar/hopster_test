(function () {

    var notesApi = function ($http, $cookies, $log, $location) {
        var tokenKey = "notesAppToken";
        var base_api_url = "/_ah/api/notes/v1";

        var getToken = function () {
            return $cookies.get(tokenKey) || ''
        };

        var removeToken = function () {
            $cookies.remove(tokenKey);
        };

        var getHeadersConfig = function (token) {
            return {
                headers: {
                    "authorization": "Bearer " + token
                }
            };
        };

        var login = function (email, passwd) {
            var login_url = base_api_url + "/login";
            var credentials = {
                "email": email,
                "passwd": passwd
            };

            return $http.post(login_url, credentials)
                .then(function (response) {
                    return response.data;
                });
        };

        var logout = function () {
            $log.info("Removing token");
            removeToken();
            $log.info("Done");
        };

        // get list of all notes
        var getNotesList = function () {
            // prepare authorization headers
            var token = getToken();
            var config = getHeadersConfig(token);
            var request_url = base_api_url + "/notes";

            return $http.get(request_url, config)
                .then(function (response) {
                    return response.data.items;
                });
        };

        // get specific note by header
        var getNote = function (header) {
            var token = getToken();
            var config = getHeadersConfig(token);
            var request_url = base_api_url + "/note/" + header;

            return $http.get(request_url, config)
                .then(function (response) {
                    return response.data;
                });
        };

        var createNote = function (header, note_text) {
            var token = getToken();
            var config = getHeadersConfig(token);
            var request_url = base_api_url + "/note/create";
            var data = {
                header: header,
                note_text: note_text
            };
            return $http.post(request_url, data, config)
                .then(function (response) {
                    return response.data;
                });
        };

        var updateNote = function (original_header, header, note_text) {
            var token = getToken();
            var config = getHeadersConfig(token);
            var request_url = base_api_url + "/note/" + original_header + "/update";
            var data = {
                header: header,
                note_text: note_text
            };
            return $http.put(request_url, data, config)
                .then(function (response) {
                    return response.data;
                });
        };

        var deleteNote = function (header) {
            var token = getToken();
            var config = getHeadersConfig(token);
            var request_url = base_api_url + "/note/" + header + "/delete";

            return $http.delete(request_url, config)
                .then(function (response) {
                    return response.data;
                });
        };

        var createDefaultUser = function () {
            var request_url = base_api_url + "/create-default-user";

            return $http.get(request_url)
                .then(function (response) {
                    return response.data;
                });
        };

        // expose api
        return {
            getToken: getToken,
            removeToken: removeToken,
            getNotesList: getNotesList,
            getNote: getNote,
            createNote: createNote,
            updateNote: updateNote,
            createDefaultUser: createDefaultUser,
            deleteNote: deleteNote,
            login: login,
            logout: logout
        };
    };


    var module = angular.module("SimpleNoteApp");
    module.factory("notesApi", notesApi);

}());