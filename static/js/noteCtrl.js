(function () {

    var app = angular.module("SimpleNoteApp");

    var GetNoteCtrl = function ($scope, notesApi, $location, $log, $routeParams) {

        var onNote = function (data) {
            $scope.note = data;
        };

        var onError = function (data) {
            $log.info("error while processing request...");
            $log.debug(data);
            if (data.status == 401) {
                // Unauthorized access. it might be that token has been expired.
                // clean up cookie and redirect to login page.
                notesApi.removeToken();
                $location.path("#/login")
            } else {
                $scope.error_message = data;
            }
        };

        notesApi.getNote($routeParams.header)
            .then(onNote, onError);

    };

    var CreateNoteCtrl = function ($scope, $location, $log, notesApi) {

        var onSuccess = function (data) {
            $scope.success_message = data;
            $location.path("/main");
        };

        var onError = function (data) {
            $log.info("error while processing request...");
            $log.debug(data);
            if (data.status == 401) {
                // Unauthorized access. it might be that token has been expired.
                // clean up cookie and redirect to login page.
                notesApi.removeToken();
                $location.path("#/login")
            } else {
                $scope.error_message = data;
            }
        };

        $scope.createNote = function (header, note_text) {
            notesApi.createNote(header, note_text)
                .then(onSuccess, onError);
        };
    };

    var UpdateNoteCtrl = function ($scope, $location, $log, notesApi, $routeParams) {
        var onUpdateSuccess = function (data) {
            $scope.success_message = data;
            $location.path("/note/" + data.header);
        };

        var onFetchSuccess = function (data) {
            $scope.note = data;
        };

        var onError = function (data) {
            $log.info("error while processing request...");
            $log.debug(data);
            if (data.status == 401) {
                // Unauthorized access. it might be that token has been expired.
                // clean up cookie and redirect to login page.
                notesApi.removeToken();
                $location.path("#/login")
            } else {
                $scope.error_message = data;
            }
        };

        var original_header = $routeParams.header;
        // prepare data to populate form contents.
        notesApi.getNote(original_header)
            .then(onFetchSuccess, onError);

        $scope.updateNote = function (header, note_text) {
            notesApi.updateNote(original_header, header, note_text)
                .then(onUpdateSuccess, onError);
        };
    };

    var DeleteNoteCtrl = function ($scope, $location, $log, notesApi, $routeParams) {
        var onSuccess = function (data) {
            $scope.success_message = data.text;
            $location.path("/main");
        };

        var onError = function (data) {
            $log.info("error while processing request...");
            $log.debug(data);
            if (data.status == 401) {
                // Unauthorized access. it might be that token has been expired.
                // clean up cookie and redirect to login page.
                notesApi.removeToken();
                $location.path("/login")
            } else {
                $scope.error_message = data;
                $location.path("/main")
            }
        };

        var header = $routeParams.header;
        $log.debug("Deleting note: " + header);
        notesApi.deleteNote(header)
            .then(onSuccess, onError);
    };

    app.controller("GetNoteCtrl", GetNoteCtrl);
    app.controller("CreateNoteCtrl", CreateNoteCtrl);
    app.controller("UpdateNoteCtrl", UpdateNoteCtrl);
    app.controller("DeleteNoteCtrl", DeleteNoteCtrl);
}());