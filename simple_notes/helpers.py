# -*- coding: utf-8 -*-

import datetime
import endpoints
from calendar import timegm
from functools import wraps
from jws.exceptions import SignatureError

from .models import SimpleNote, UserRecord
from .utils import to_jwt, from_jwt

NOTE_NOTE_FOUND_MSG = 'Note with header: {} not found'
UNAUTHORIZED_ERROR_MSG = 'Unauthorized access is not allowed, please login.'
TOKEN_SECRET = 'top_secret_token'


def model_to_form(model_instance, form_class):
    """
    Build protoRPC message instance from ndb model instance.
    :param model_instance: SimpleNote instance
    :param form_class: message, custom protoRPC message class
    :return: form_class instance
    """
    form_instance = form_class(**model_instance.to_dict())
    # validate values
    form_instance.check_initialized()
    return form_instance


def convert_request_message_to_dict(request_message):
    """
    Convert request protoRPC message to dict.
    :param request_message: request with proto RPC message
    :return: dict
    """
    return {field.name: getattr(request_message, field.name)
            for field in request_message.all_fields()}


def validate_required_data(data_dict, required_fields):
    """
    Validate request data to ensure that all required fields were passed.
    Returns list of error messages.
    :param data_dict: dict, data to be validated
    :param required_fields: iterable, fields that are considered as required
    :return: list, found errors
    """
    empty_values = (None, [], '')
    errors = []
    for field_name in required_fields:
        field_value = data_dict.get(field_name, None)
        if field_value in empty_values:
            error_message = "Field {} is required.".format(field_name)
            errors.append(error_message)
    return errors


def get_note_by_header(header, raise_errors=True):
    """
    Lookup note by note header, if note not found and
    raise_errors=True - will raise endpoints.NotFoundException
    :param header: str, note header
    :param raise_errors: bool, if exception should be raised
    :return: SimpleNote instance or None
    """
    note_query = SimpleNote.query(SimpleNote.header == header)
    note = note_query.get()
    # check the results
    if not note and raise_errors:
        error_msg = NOTE_NOTE_FOUND_MSG.format(header)
        raise endpoints.NotFoundException(error_msg)
    return note


def update_model_instance(instance, data):
    """
    Update ndb model instance helper method to set attributes provided
    in the `data` dict.
    Note that this function doesn't do any database operations.
    :param instance: ndb model instance
    :param data: dict, {property_name: value, ...} data to update instance
    :return: instance with updated properties.
    """
    for property_name, value in data.items():
        setattr(instance, property_name, value)
    return instance


def _datetime_to_timestamp(dt_instance):
    """
    Convert datetime instance to timestamp
    :param dt_instance: datetime.datetime instance
    :return: int, timestamp
    """
    return timegm(dt_instance.utctimetuple())


def generate_token(email, extra_values=None):
    """
    Generate JWT token which will contain user email, issued date time
    and any extra_values.
    :param email: str, user email
    :param extra_values: dict, any extra values that should be added to
        the token.
    :return: str, generated token
    """
    if extra_values is None:
        extra_values = {}
    iat = datetime.datetime.utcnow()
    iat_timestamp = _datetime_to_timestamp(iat)
    exp = iat + datetime.timedelta(hours=1)
    exp_timestamp = _datetime_to_timestamp(exp)
    defaults = {
        'iat': iat_timestamp,
        'exp': exp_timestamp,
        'email': email
    }
    # update extra values with generated defaults to ensure that
    # extra_values won't override the defaults
    extra_values.update(defaults)
    token = to_jwt(extra_values, key=TOKEN_SECRET)
    return token


def token_is_valid(token):
    """
    Check token validity.
    :param token: str, token
    :return: bool, True or False
    """
    try:
        decoded_token = from_jwt(token, key=TOKEN_SECRET)
    except (SignatureError, ValueError):
        # don't do further checks
        return False

    now = datetime.datetime.utcnow()
    now_timestamp = _datetime_to_timestamp(now)
    expired = decoded_token.get('exp') < now_timestamp
    iat_in_future = decoded_token.get('iat') > now_timestamp
    email = decoded_token.get('email')
    # check base part of the token
    if email is None or expired or iat_in_future:
        # no need to do the query.
        return False
    # consider token as valid only if all values are valid and user exists
    user = UserRecord.query().filter(UserRecord.email == email)
    if user:
        return True
    return False


def endpoint_login_required(func):
    """
    Login required decorator. Will raise
    `endpoints.UnauthorizedException` if token is not valid.
    :param func: method to wrap
    :return: wrapped func/method
    """
    @wraps(func)
    def wrapper(self, request, *args, **kwargs):
        header = self.request_state.headers.get('authorization')
        if header:
            auth_type, token = header.split()
        else:
            auth_type = token = None
        auth_type_is_valid = auth_type == 'Bearer'
        if not auth_type_is_valid or token is None or not token_is_valid(token):
            raise endpoints.UnauthorizedException(UNAUTHORIZED_ERROR_MSG)
        result = func(self, request, *args, **kwargs)
        return result
    return wrapper
