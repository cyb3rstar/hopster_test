from protorpc import messages
from google.appengine.ext import ndb

# fields that are required for simple note creation
SIMPLE_NOTE_REQUIRED_FIELDS = (
    'header',
    'note_text',
)


class SimpleNote(ndb.Model):
    """
    Simple note model to store notes.
    """
    header = ndb.StringProperty()
    note_text = ndb.TextProperty()


class SimpleNoteForm(messages.Message):
    """
    Note form to transfer notes from client to backend and vice versa.
    """
    header = messages.StringField(1)
    note_text = messages.StringField(2)


class SimpleNoteUpdateForm(messages.Message):
    """
    Note form to update existing note. old_header is used to lookup
    the note, header and note_text is used to update the note.
    """
    old_header = messages.StringField(1)
    header = messages.StringField(2)
    note_text = messages.StringField(3)


class SimpleNoteForms(messages.Message):
    """
    Message definition for list of notes.
    """
    items = messages.MessageField(SimpleNoteForm, 1, repeated=True)


class GeneralTextMessage(messages.Message):
    """
    Message to send any text back to the user.
    """
    text = messages.StringField(1)


class UserRecord(ndb.Model):
    """
    User model to save credentials.
    """
    email = ndb.StringProperty()
    passwd_hash = ndb.StringProperty()


class UserRecordForm(messages.Message):
    """
    User credentials from to get data from frontend api request.
    """
    email = messages.StringField(1)
    passwd = messages.StringField(2)


class UserTokenMessage(messages.Message):
    """
    Message to send generated token back to user.
    """
    token = messages.StringField(1)
