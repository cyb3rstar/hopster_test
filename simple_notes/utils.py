# -*- coding: utf-8 -*-
import jws


def to_jwt(claim, key, algo='HS256'):
    """
    Encode JWT token
    :param claim: dict, claim
    :param key: str, secret key to encode token
    :param algo: str, algorithm
    :return: str, encoded token
    """
    header = {'typ': 'JWT', 'alg': algo}
    token = '.'.join([
        jws.utils.encode(header),
        jws.utils.encode(claim),
        jws.sign(header, claim, key)
    ])
    return token


def from_jwt(jwt, key):
    """
    Returns the decoded claim on success, or throws exception on error
    :param jwt: str, encoded token
    :param key: str, secret
    :return: dict, decoded token
    """
    (header, claim, sig) = jwt.split('.')
    header = jws.utils.from_base64(header)
    claim = jws.utils.from_base64(claim)
    jws.verify(header, claim, sig, key, is_json=True)
    return jws.utils.from_json(claim)
