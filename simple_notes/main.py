# -*- coding: utf-8 -*-

import endpoints
from hashlib import sha256
from protorpc import remote, message_types, messages

from .models import (
    SimpleNote, SimpleNoteForm, SimpleNoteForms, SIMPLE_NOTE_REQUIRED_FIELDS,
    GeneralTextMessage, UserRecord, UserRecordForm, UserTokenMessage,
)
from .helpers import (
    get_note_by_header, update_model_instance, model_to_form,
    validate_required_data, convert_request_message_to_dict, generate_token,
    endpoint_login_required,
)

API_EXPLORER_CLIENT_ID = endpoints.API_EXPLORER_CLIENT_ID

# test user credentials
TEST_USER_EMAIL = 'test@example.com'
TEST_USER_PASSWD = 'test'

# lookup notes by title
SIMPLE_NOTE_GET_REQUEST = endpoints.ResourceContainer(
    message_types.VoidMessage,
    note_header=messages.StringField(1)
)

SIMPLE_NOTE_POST_REQUEST = endpoints.ResourceContainer(
    SimpleNoteForm,
    note_header=messages.StringField(1)
)


@endpoints.api(
    name='notes',
    version='v1',
    allowed_client_ids=[API_EXPLORER_CLIENT_ID])
class SimpleNotesApi(remote.Service):

    def _create_simple_note(self, request):
        """
        Create SimpleNote db record based on request data
        :param request: protoRPC message with simple note data
        :return:
        """
        simple_note_data = convert_request_message_to_dict(request)
        errors = validate_required_data(
            data_dict=simple_note_data,
            required_fields=SIMPLE_NOTE_REQUIRED_FIELDS)
        # if required fields were empty - raise an error.
        if errors:
            error_message = '\n'.join(errors)
            raise endpoints.BadRequestException(error_message)
        simple_note = SimpleNote(**simple_note_data)
        simple_note.put()
        return simple_note

    @endpoints.method(
        request_message=message_types.VoidMessage,
        response_message=SimpleNoteForms,
        path='notes',
        http_method='GET',
        name='get-notes-list')
    @endpoint_login_required
    def get_notes_list(self, request):
        queryset = SimpleNote.query()
        items = [model_to_form(note, SimpleNoteForm)
                 for note in queryset]
        return SimpleNoteForms(items=items)

    @endpoints.method(
        request_message=SIMPLE_NOTE_GET_REQUEST,
        response_message=SimpleNoteForm,
        path='note/{note_header}',
        http_method='GET',
        name='get-note')
    @endpoint_login_required
    def get_note(self, request):
        lookup_header = request.note_header
        note = get_note_by_header(lookup_header)
        return model_to_form(note, SimpleNoteForm)

    @endpoints.method(
        request_message=SimpleNoteForm,
        response_message=SimpleNoteForm,
        path='note/create',
        http_method='POST',
        name='create-note')
    @endpoint_login_required
    def create_note(self, request):
        created_note = self._create_simple_note(request)
        return model_to_form(created_note, SimpleNoteForm)

    @endpoints.method(
        request_message=SIMPLE_NOTE_POST_REQUEST,
        response_message=SimpleNoteForm,
        path='note/{note_header}/update',
        http_method='PUT',
        name='update-note')
    @endpoint_login_required
    def update_note(self, request):
        old_header = request.note_header
        new_header = request.header
        note_text = request.note_text
        update_kwargs = {'note_text': note_text}
        if old_header != new_header:
            update_kwargs['header'] = new_header
        note = get_note_by_header(old_header)
        updated_note = update_model_instance(note, update_kwargs)
        # store updated note in to the data base
        updated_note.put()
        return model_to_form(updated_note, SimpleNoteForm)

    @endpoints.method(
        request_message=SIMPLE_NOTE_GET_REQUEST,
        response_message=GeneralTextMessage,
        path='note/{note_header}/delete',
        http_method='DELETE',
        name='delete-note')
    @endpoint_login_required
    def delete_note(self, request):
        header = request.note_header
        note = get_note_by_header(header)
        note.key.delete()
        message = "Successfully deleted note: {}".format(header)
        return GeneralTextMessage(text=message)

    #  --- Authentication ---

    # simplification to create default user
    @endpoints.method(
        request_message=message_types.VoidMessage,
        response_message=GeneralTextMessage,
        path='create-default-user',
        http_method='GET',
        name='create-default-user')
    def create_default_user(self, request):
        users = UserRecord.query().fetch()
        # create test user if there is no user at all
        if len(users) == 0:
            passwd_hash = sha256(TEST_USER_PASSWD).hexdigest()
            default_user = UserRecord(
                email=TEST_USER_EMAIL,
                passwd_hash=passwd_hash)
            # create user record
            default_user.put()
            result_smg = 'Default user was created successfully.'
        else:
            result_smg = 'Default user already exists'
        return GeneralTextMessage(text=result_smg)

    @endpoints.method(
        request_message=UserRecordForm,
        response_message=UserTokenMessage,
        path='login',
        http_method='POST',
        name='login')
    def login(self, request):
        email = request.email
        passwd = request.passwd
        if passwd:
            calculated_passwd_hash = sha256(passwd).hexdigest()
            user = UserRecord.query().filter(UserRecord.email == email).get()
        else:
            calculated_passwd_hash = None
            user = None
        if not user or not user.passwd_hash == calculated_passwd_hash:
            auth_error_msg = 'Authentication error. Please check credentials.'
            raise endpoints.UnauthorizedException(auth_error_msg)
        token = generate_token(user.email)
        return UserTokenMessage(token=token)


api = endpoints.api_server([SimpleNotesApi])
